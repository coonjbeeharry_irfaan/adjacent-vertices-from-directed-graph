/**
 * This file gives the implementation of a Graph.
 * 
 * YOUR TASKS : Implement methods (1) multiplyMatrix and (2) getNeighbors of the class Graph.
 */

import java.util.ArrayList;
import java.util.Arrays;

public class Graph {
	/**
	 * This class implements a directed graph, representing it using an adjacency matrix.
	 * 
	 */	
	private char[] vertices;
	private static int[][] adjacencyMatrix;
	int N=5;
	
	public Graph(char[] vertices){
		int numVertices = vertices.length;
		this.vertices = vertices;
		adjacencyMatrix = new int[numVertices][numVertices];
	}
	
	public void addEdge(char from, char to){
		/**
		 * Adds an edge (from, to) to the graph creating a path from vertex 'from' to vertex 'to'.
		 */
		adjacencyMatrix[new String(vertices).indexOf(from)][new String(vertices).indexOf(to)] = 1;
	}
	
	public void removeEdge(char from, char to){
		/**
		 * Removes the edge (from, to) from the graph.
		 */
		adjacencyMatrix[new String(vertices).indexOf(from)][new String(vertices).indexOf(to)] = 0;
	}
	
	public static int[][] multiplyMatrix(int[][] matrixA, int[][] matrixB){
		/**
		 * Multiplies 2 two-dimensional matrices, that it takes as argument, and returns the resulting matrix.
		 * 
		 * YOUR TASK: Write the appropriate code to multiply matrixA and matrixB, store the result in matrix result,
		 * and return matrix result at the end.
		 * 
		 */
		int[][] result =new int[5][5];

		for (int i = 0; i < matrixA.length; i++) { 
        for (int j = 0; j < matrixB[0].length; j++) { 
        for (int k = 0; k < matrixA[0].length; k++) { 
            result[i][j] += matrixA[i][k] * matrixB[k][j];
        }
    }
}
		
	/*	
		for(int i = 0; i < 5; i++)
             {
        for(int j = 0; j < 5; j++)
           {
         System.out.printf("%5d ", result[i][j]);
         }
         System.out.println();
             }
		*/
		
		/**
		 * WRITE YOUR CODE HERE
		 */
		
		return result;
	}
	
	public String[] getNeighbors(int distance){
		/**
		 * For every vertex in the graph, identifies and returns the list of vertices at a distance 'distance' from it 
		 *  
		 * YOUR TASK: Write the appropriate code that will (i) for every vertex, find the list of vertices a the distance
		 * passed as argument from it, and (2) return the lists of 'neighbors' as an array of String; the neighbors for a 
		 * vertex is returned as a comma-separated String (a comma separating the label of each neighbor for the vertex).
		 * 
		 * For example, if there are 3 vertices A, B and C, and distance = 2. It will identify the vertices with a path of 
		 * length 2 from vertex A. It will do the same for vertices B and C. Suppose that there are the following paths 
		 * of length 2 in the graph: A to B, A to C, B to C and C to A. The function will then return {"B, C", "C", "A}.
		 * 
		 */
		String[] neighborList = new String[vertices.length];
		Arrays.fill(neighborList, "");				// initialize all elements with empty string
		
		int[][] temp = adjacencyMatrix;
		
		for (int i=0;i<distance-1;i++) {
			temp = multiplyMatrix(temp,adjacencyMatrix);
		}
		
		for(int i=0; i<vertices.length;i++) {
		 	for (int j=0; j<temp.length; j++) {
		 		if( temp[i][j]>0 ) {
		 			neighborList[i] += vertices[j]+",";
		 	}
		
		}
		 	neighborList[i] = neighborList[i].substring(0, neighborList[i].length() - 1);
	}
		
		
		
		return neighborList;
	}
	
	
	
	
	public String toString(){		
		int numVertices = vertices.length;
		String tmp = "Vertices: ";
		
		for(char vertex: vertices) { tmp += vertex + "\t"; }
		
		tmp += "\nEdges: ";
		for(int i = 0; i < numVertices; i++){
			for(int j = 0; j < numVertices; j++){
				if(adjacencyMatrix[i][j] == 1) { tmp += "(" + vertices[i] + ", " + vertices[j] + ")\t"; }
			}
		}
		return tmp;
	}
} //end class Graph

