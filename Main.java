/**
 * Main class of the Java program. 	
 * 
 */


public class Main {

    public static void main(String[] args) {
        
        // we print a heading and make it bigger using HTML formatting
        System.out.println("<h4>-- Graph --</h4>");
        
        // create a Graph object, print its details, and call method getNeighbors to find, for every vertex, the list 
        // of vertices at a distance 'pathLength' from it (distance is im terms of number of edges)
        char[] vertices = {'A', 'B', 'C', 'D', 'E'};
		Graph myGraph = new Graph(vertices);
		myGraph.addEdge('A', 'B');
		myGraph.addEdge('A', 'E');
		myGraph.addEdge('B', 'D');
		myGraph.addEdge('C', 'B');
		myGraph.addEdge('D', 'A');
		myGraph.addEdge('E', 'B');
		myGraph.addEdge('E', 'C');
		System.out.println("Graph details:-");
		System.out.println(myGraph);
		System.out.println();
		
		int pathLength = 1;
		String[] neighborList = myGraph.getNeighbors(pathLength);
		System.out.println("For each vertex, the list of vertices at distance " + pathLength + " from it:-");
		for(int i = 0; i < vertices.length; i++){
			System.out.println(vertices[i] + ": " + neighborList[i]);
		}

	
	} // end main
}
